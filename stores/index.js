
module.exports = function createStores (wsClient) {
  return function (state, emitter) {
    Object.assign(state, {
      files: {},
      cats: {},
      wishlist: [],
      downloads: {},
      uploads: {},
      swarms: {
        connected: [],
        disconnected: [],
        currentStateUnkown: []
      },
      connectionSettings: {
        host: wsClient.host,
        port: wsClient.port
      },
      searchResults: {},
      downloadedFiles: {},
      expandedDirs: {}
    })

    const messageHandlers = {
      ls ({ entries }, request) {
        if (request.ls && request.ls.recursive) {
          // Treat as search result
          const index = JSON.stringify(request.ls)
          state.searchResults[index] = state.searchResults[index] || []
          for (const result of entries) {
            state.searchResults[index].push(result)
          }
        } else {
          state.files[request.ls.path] = entries
          console.log(state.expandedDirs, state.expandedDirs[request.ls.path], request.ls.path)
          if (state.expandedDirs[request.ls.path] === 'expanding') {
            state.expandedDirs[request.ls.path] = true
          }
        }
        emitter.emit('render')
      },

      wishlist ({ item }) {
        state.wishlist = item
        emitter.emit('render')
      },

      swarm ({ connected, disconnected }) {
        state.swarms = {
          connected: connected,
          disconnected: disconnected,
          currentStateUnkown: []
        }
        emitter.emit('render')
      },

      // TODO what happens when the req is not there (path is undefined)
      download ({ filePath, bytesRead, totalBytesRead }, { path }, id) {
        // const path = message.request.download.path
        const pathOfDownloadingFile = filePath || path
        state.downloads[pathOfDownloadingFile] = state.downloads[pathOfDownloadingFile] || {}
        state.downloads[pathOfDownloadingFile].bytesRead = bytesRead
        state.downloads[path] = state.downloads[path] || {}
        state.downloads[path].totalBytesRead = totalBytesRead
        state.downloads[pathOfDownloadingFile].id = id
        state.downloads[path].id = id
        emitter.emit('render')
      },

      upload (upload, request, id) {
        state.uploads[id] = upload
        emitter.emit('render')
      },

      peerConnected ({ self, name }) {
        if (self) {
          state.name = name
          emitter.emit('render')
        } else {
          wsClient.request({ ls: { path: '/' } })
        }
      },

      peerDisconnected ({ self, name }) {
        state.files['/'] = state.files['/'].filter(entry => entry.name !== name)
        for (const path of Object.keys(state.files)) {
          if (path.startsWith(name + '/')) delete state.files[path]
        }
        emitter.emit('render')
      },

      downloaded ({ downloadedFiles }) {
        for (const downloadedFile of downloadedFiles) {
          state.downloadedFiles[downloadedFile.filePath] = downloadedFile
        }
        emitter.emit('render')
      },

      endResponse (response, request, id) {
        if (state.uploads[id]) {
          state.uploads[id].complete = true
          emitter.emit('render')
        } else {
          for (const download of Object.values(state.downloads)) {
            if (download.id === id) {
              download.complete = true
              emitter.emit('render')
            }
          }
        }
      }
    }

    wsClient.on('message', (message) => {
      emitter.emit(message.id.toString(), message)

      if (message.success) {
        const command = Object.keys(message.success)[0]
        if (typeof messageHandlers[command] === 'function') {
          messageHandlers[command](message.success[command], message.request, message.id)
        }
      }
      // TODO handle message.err
    })

    wsClient.on('error', (err) => {
      state.connectionError = err
      emitter.emit('render')
    })

    wsClient.on('close', (event) => {
      state.connectionError = event
      emitter.emit('render')
    })

    wsClient.on('open', () => {
      state.connectionError = false
      emitter.emit('render')
    })

    emitter.on('request', (request) => {
      console.log('REQUEST', request)
      wsClient.request(request)
    })

    emitter.on('showMedia', (filepath, readStream, opts) => {
      wsClient.request({ cat: { path: filepath } }).then((id) => {
        emitter.on(id.toString(), (message) => {
          if (message.success) {
            if (message.success.cat) {
              console.log('Pushing to stream')
              readStream.push(Buffer.from(message.success.cat.data))
            } else if (message.success.endResponse) {
              console.log('Closing stream')
              readStream.push(null)
            }
          }
        })
      })
    })

    emitter.on('download', (path) => {
      wsClient.request({ download: { path } }).then((id) => {
        state.downloads[path] = state.downloads[path] || {}
        state.downloads[path].requested = true
        state.downloads[path].id = id
      })
    })

    emitter.on('search', (searchterm) => {
      emitter.emit('request', { ls: { searchterm, recursive: true } })

      const index = JSON.stringify({ searchterm, recursive: true })
      state.searchResults[index] = state.searchResults[index] || []
      state.selectedSearchResult = index
      emitter.emit('replaceState', '#search')
      emitter.emit('render')
    })

    // Begin by reading root dir, and wishlist
    wsClient.request({ ls: { path: '/' } })
    wsClient.request({ wishlist: {} })
  }
}
