# harddrive-party-ui

Simple web interface, built with [choo](https://www.choo.io/), for [harddrive-party](https://github.com/ameba23/hdp).

### TODO

- [ ] Represent downloaded directories in `state.downloadedFiles`
- [ ] Show spinner when waiting for initial ls '/' result
- [ ] Choose dir to share
- [ ] uploads not displayed
