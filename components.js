const h = require('hyperscript')

module.exports = {
  spinner () {
    return h('div.spinner-border.spinner-border-sm', { role: 'status' },
      h('span.sr-only', 'Loading...')
    )
  },

  progressBar (perc) {
    return h('div.progress',
      h('div.progress-bar.bg-success',
        {
          role: 'progressbar',
          style: `width: ${perc}%`,
          'aria-valuenow': perc,
          'aria-valuemin': '0',
          'aria-valuemax': '100'
        },
        perc
      )
    )
  }
}
