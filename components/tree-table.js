const h = require('hyperscript')
const displayFiles = require('./item')

// Display file heirarchy in a tree-table

module.exports = function (state, emit, baseDir = '/', ownSharesOnly) {
  const filter = ownSharesOnly || baseDir !== '/'
    ? () => true
    : (file) => file.name !== state.name

  const filesToDisplay = state.files[baseDir]
    ? state.files[baseDir]
        .filter(filter)
    : []

  if (!state.files[baseDir]) emit('request', { ls: { path: baseDir } })

  return h('table.table.table-hover.table-sm',
    // h('thead',
    //   h('tr',
    //     h('th', { scope: 'row' }, 'Name'),
    //     h('th', { scope: 'row' }, 'Size')
    //   )
    // ),
    h('tbody',
      filesToDisplay.length
        ? filesToDisplay.map(displayFiles(state, emit, baseDir, 0))
        : h('p', ownSharesOnly ? 'No shared files' : 'No connected peers')
    )
  )
}
