const h = require('hyperscript')
const path = require('path')
const icons = require('../icons')
const { readableBytes } = require('../util')
const { progressBar, spinner } = require('../components')
const createComponentEmbedMedia = require('./embed-media')

module.exports = function displayFiles (state, emit, baseDir, depth = 0) {
  console.log('displayfiles', baseDir, depth)
  const embedMedia = createComponentEmbedMedia(state, emit)
  return function (file) {
    const fullPath = baseDir ? getFullPath(baseDir, file.name) : file.name
    const isOwnFile = fullPath.startsWith(state.name + '/')

    if (file.isDir) {
      const downloadState = isOwnFile || baseDir === '/'
        ? undefined
        : state.downloads[fullPath]
          ? `Downloaded ${readableBytes(state.downloads[fullPath].totalBytesRead)} ${state.downloads[fullPath].complete ? 'complete' : ''}`
          : state.wishlist.find(i => fullPath.startsWith(i))
            ? 'Queued for download'
            : state.downloadedFiles[fullPath]
              ? `Downloaded ${state.downloadedFiles[fullPath].timestamp}`
              : h('button.btn.btn-sm.bit-light', {
                onclick: downloadItem(fullPath),
                title: 'Download'
              }, icons.use('arrow-down-circle'))

      return [
        h('tr.table-secondary',
          h('td',
            h('span', { style: `margin-left: ${depth * 1.5}rem` },
              h('button.btn.btn-sm.bit-light', {
                onclick: () => {
                  console.log('toggle expand', file.name, state.expandedDirs[fullPath])
                  if (state.expandedDirs[fullPath]) {
                    state.expandedDirs[fullPath] = false
                    emit('render')
                  } else {
                    if (state.files[fullPath]) {
                      state.expandedDirs[fullPath] = true
                      emit('render')
                    } else {
                      state.expandedDirs[fullPath] = 'expanding'
                      emit('request', { ls: { path: fullPath } })
                      emit('render')
                    }
                  }
                },
                title: file.name
              },
              icons.use(baseDir === '/' ? 'person' : 'folder'),
              h('code.text-reset.ml-1', file.name),
              state.expandedDirs[fullPath] === 'expanding'
                ? spinner()
                : undefined
              )
            )
          ),
          h('td',
            h('small', readableBytes(file.size)),
            downloadState
          )
        )].concat(state.expandedDirs[fullPath] === true
        ? state.files[fullPath].map(displayFiles(state, emit, fullPath, depth + 1))
        : [])
    } else {
      const haveLocally = isOwnFile || state.downloadedFiles[fullPath]

      const downloadState = isOwnFile || baseDir === '/'
        ? undefined
        : haveLocally
          ? `Downloaded ${new Date(state.downloadedFiles[fullPath].timestamp).toLocaleString()}`
          : state.downloads[fullPath]
            ? downloadProgress(state.downloads[fullPath].bytesRead, file.size)
            : state.wishlist.find(i => fullPath.startsWith(i))
              ? 'Queued for download'
              : h('button.btn.btn-sm.bit-light', {
                onclick: downloadItem(fullPath),
                title: 'Download'
              }, icons.use('arrow-down-circle'))

      return h(
        `tr${(haveLocally && !isOwnFile) ? '.table-success' : ''}`,
        h('td',
          h('span',
            { style: `margin-left: ${depth * 1.5}rem` },
            icons.use('file'),
            h('code.text-reset', file.name, ' '),
            haveLocally ? embedMedia(fullPath) : undefined
          )
        ),
        h('td',
          h('small.mx-2', readableBytes(file.size)),
          downloadState
          // h('button', { onclick: showMedia(fullPath) }, 'show media'),
          // h('div', { id: 'preview' + cleanPathString(fullPath) })
        )
      )
    }
    // function expandDir (dir, path) {
    //   return function () {
    //     console.log('toggle expand', dir.name)
    //     if (dir.expanded) {
    //       dir.expanded = false
    //       emit('render')
    //     } else {
    //       dir.expanded = true
    //       if (state.files[path]) {
    //         emit('render')
    //       } else {
    //         emit('request', { ls: { path } })
    //       }
    //     }
    //   }
    // }
    function downloadItem (path) {
      return function () {
        emit('download', path)
      }
    }
  }
}

function getFullPath (baseDir, dirname) {
  const fullPath = path.join(baseDir, dirname)
  return fullPath[0] === '/' ? fullPath.slice(1) : fullPath
}

function downloadProgress (bytesRead, size) {
  const percentage = Math.round(bytesRead / size * 100)
  return h('span',
    `Downloaded ${readableBytes(bytesRead)} / ${readableBytes(size)} (${percentage}%)`,
    progressBar(percentage)
  )
}
