const h = require('hyperscript')
const icons = require('../icons')
const IMAGE_TYPES = ['png', 'jpeg', 'jpg', 'gif']
const AUDIO_TYPES = ['mp3', 'wav', 'ogg', 'flac'] // flac?
const VIDEO_TYPES = ['mp4', 'webm']
const EMBED_TYPES = ['pdf', 'txt', 'html'] // epub?
// AUDIO_VIDEO_TYPES: ['audio/mpeg', 'audio/ogg', 'audio/webm', 'audio/wav', 'video/mp4', 'video/webm']

const playerOptions = { controls: true, autoplay: true }

module.exports = function createComponentEmbedMedia (state, emit) {
  const url = `http://${state.connectionSettings.host}:${state.connectionSettings.port}`
  return function embedMedia (fullPath) {
    const isOwnFile = fullPath.startsWith(state.name + '/')
    const src = isOwnFile
      ? `${url}/shares/${fullPath.slice(state.name.length + 1)}`
      : `${url}/downloads/${fullPath}`

    const extension = fullPath.split('.').pop().toLowerCase()
    if (IMAGE_TYPES.includes(extension)) return h('img', { src, width: 200, alt: src })
    return embedPlayableMedia(state, emit, src, extension)
    // return h('a.btn.btn-outline-secondary', { href: src, target: '_blank' }, h('small', 'Open in browser'))
  }

  function embedPlayableMedia (state, emit, src, extension) {
    if (state.itemPlaying === src) {
      state.itemPlaying = false // only play once
      // return h(type.split('/')[0], playerOptions, h('source', { src, type }))
      if (AUDIO_TYPES.includes(extension)) return h('audio', playerOptions, h('source', { src }))
      return h('embed', { src, width: 600 })
    }

    function onclick () {
      state.itemPlaying = src
      emit('render')
    }

    if (EMBED_TYPES.includes(extension)) {
      return h('button.btn',
        { onclick, title: 'Preview media' },
        icons.use('file-text')
      )
    }
    if (AUDIO_TYPES.includes(extension) || VIDEO_TYPES.includes(extension)) {
      return h('button.btn',
        { onclick, title: 'Play media' },
        icons.use('caret-right-square'))
    }
    return h('a', { href: src, title: 'Download with browser' },
      h('button.btn', icons.use('file-arrow-down')))
  }
}
