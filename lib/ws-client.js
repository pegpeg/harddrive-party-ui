const { ClientMessage, ServerMessage } = require('./messages')
const EventEmitter = require('events')

// Send and handle messages over Websocket

module.exports = class WsClient extends EventEmitter {
  constructor (host, port) {
    super()
    this.host = host || 'localhost'
    this.port = port || 2323
    this.url = `ws://${this.host}:${this.port}`
    this.requests = {}
    this._connect()
  }

  _connect () {
    this.client = new WebSocket(this.url)
    this.client.binaryType = 'arraybuffer'

    const self = this
    this.client.addEventListener('error', (err) => {
      console.log('Error from ws', err)
      self.emit('error', err)
    })

    this.client.addEventListener('close', (event) => {
      console.log('Ws connection closed')
      self.connected = false
      self.emit('close', event)
      setTimeout(() => { self._connect() }, 5000)
    })

    this.client.addEventListener('message', (event) => {
      let message
      try {
        // message = ServerMessage.read(new Pbf(event.data))
        message = ServerMessage.decode(Buffer.from(event.data))
        console.log('ws:', message) // temp

        // if this is the final message, remove it from requests
        if (message.success && message.success.endResponse) {
          delete this.requests[message.id]
        }

        if (this.requests[message.id]) {
          message.request = this.requests[message.id]
        } else {
          this.requests[message.id] = message
        }

        self.emit('message', message)
      } catch (err) {
        console.log('Error parsing ws message', err) // TODO
      }
    })

    this._waitForConnect()
  }

  async request (reqMessage) {
    reqMessage.id = this._randomId()
    // TODO add timeout
    if (!this.connected) await this._waitForConnect()
    this.client.send(ClientMessage.encode(reqMessage))
    this.requests[reqMessage.id] = reqMessage
    return reqMessage.id
  }

  async _waitForConnect () {
    const self = this
    return new Promise((resolve, reject) => {
      self.client.addEventListener('open', () => {
        self.connected = true
        self.emit('open')
        resolve()
      })
    })
  }

  _randomId () {
    let u = new Uint32Array(1)
    window.crypto.getRandomValues(u)
    return u[0]
  }
}
