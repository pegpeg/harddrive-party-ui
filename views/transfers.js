const h = require('hyperscript')
const TITLE = 'harddrive-party - transfers'
const basic = require('./basic')
const { readableBytes } = require('../util')
const createComponentEmbedMedia = require('../components/embed-media')

module.exports = view

function view (state, emit) {
  if (state.title !== TITLE) emit(state.events.DOMTITLECHANGE, TITLE)
  const embedMedia = createComponentEmbedMedia(state, emit)
  return basic(state, emit,
    h('div',
      h('div.row',
        h('div.col',
          h('h3', 'Queued for download'),
          h('small.text-muted', 'Will be downloaded when a peer who has the file is online and has a download-slot free'),
          // h('ul', state.requests.sort(namesSort).map(displayWishListItem)),
          h('table.table.table-sm.table-hover',
            h('thead',
              h('tr',
                h('th', 'From'),
                h('th', 'Filename'),
                h('th', ''),
                h('th', '')
              )
            ),
            h('tbody',
              state.wishlist.map(displayWishListItem)
            )
          ),
          // h('p', JSON.stringify(state.wishlist)),
          h('p', JSON.stringify(state.downloads)),
          h('h3', 'Downloaded'),
          h('table.table',
            h('thead',
              h('tr',
                h('th', 'Filename'),
                h('th', 'From'),
                h('th', 'Size'),
                h('th', 'When')
              )
            ),
            h('tbody',
              Object.values(state.downloadedFiles)
                .sort((fileA, fileB) => {
                  if (fileA.timestamp > fileB.timestamp) return -1
                  if (fileA.timestamp < fileB.timestamp) return 1
                  return 0
                })
                .map(displayDownloadedFile)
            )
          ),
          h('p', JSON.stringify(state.downloadedFiles))
        ),
        h('div.col',
          h('h3', 'Requests received'),
          h('p', JSON.stringify(state.uploads)),
          // state.wsEvents.uploadQueue
          //   ? h('ul', state.wsEvents.uploadQueue.map(displayUploadQueueItem))
          //   : undefined,
          // state.wsEvents.upload
          //   ? h('ul', Object.values(state.wsEvents.upload).map(displayUploadingItem))
          //   : undefined,
          h('h3', 'Uploaded'),
          h('table.table',
            h('thead',
              h('tr',
                h('th', 'Filename'),
                h('th', 'To'),
                h('th', '')
              )
            ),
            h('tbody'
              // state.uploads.map(displayUploadedFile)
            )
          )
        )
      )
    )
  )

  function displayWishListItem (fullPath) {
    const from = fullPath.split('/')[0]
    const subPath = fullPath.slice(from.length)
    return h('tr',
      h('td', h('code.text-reset', from)),
      h('td', h('code.text-reset', subPath)),
      h('td', state.downloads[fullPath] ? downloadProgress(state.downloads[fullPath].totalBytesRead) : 'Queued'),
      h('td', h('button.btn.btn-sm.btn-outline-danger', { title: 'Cancel request' }, 'Cancel'))
    )
    // TODO { onclick: unrequest(filePath) },
  }

  function downloadProgress (bytesRead) {
    return h('span', `Downloaded ${readableBytes(bytesRead)}`)
    // const percentage = Math.round(bytesRead / size * 100)
    // return h('span',
    //   `Downloaded ${readableBytes(bytesRead)} / ${readableBytes(size)} (${percentage}%)`,
    //   progressBar(percentage)
    // )
  }

  function displayDownloadedFile ({ filePath, localPath, timestamp, size }) {
    const from = filePath.split('/')[0]
    const path = filePath.slice(from.length + 1)
    return h('tr',
      h('td',
        h('code.text-reset', path),
        embedMedia(filePath)
      ),
      h('td', from),
      h('td', readableBytes(size)),
      h('td', new Date(timestamp).toLocaleString())
    )
  }
}

// function getTopLevelDirs (files) {
//   const topLevelDirs = {} // new Map()
//   for (const file in files) {
//     // if (!Array.isArray(file.filename)) file.filename = [file.filename]
//     // for (const f in file.filename) {
//     //   // topLevelDirs.set(f.split('/')[0], file.hash)
//     //   if (!topLevelDirs[f.split('/')[0]]) topLevelDirs[f.split('/')[0]] = []
//     //   topLevelDirs[f.split('/')[0]].push(file.hash)
//     // }
//     topLevelDirs[file.hash] = 'bop'
//   }
//   return JSON.stringify(topLevelDirs)
// }

// function displayUploadingItem (item) {
//   const bytesSent = item.bytesSent || 0
//   const size = item.size || 0
//   const percentage = Math.round(bytesSent / size * 100)
//   return h('li',
//     displayPeer(item.to), ' ',
//     h('a', { href: `#files/${item.sha256}` }, h('code.text-reset', item.filename)),
//     ` ${readableBytes(bytesSent)} of ${readableBytes(size)} ${percentage}% ${item.kbps} kbps`,
//     progressBar(percentage)
//   )
// }
//
// function displayUploadQueueItem (item) {
//   return h('li',
//     displayPeer(item.to), ' ',
//     h('a', { href: `#files/${item.sha256}` }, h('code.text-reset', item.baseDir, '/', item.filePath)),
//     (item.offset > 0 || item.length > 0)
//       ? `Start: ${item.offset} Length: ${item.length}`
//       : undefined
//   )
// }

// function displayDownloadingFile (name) {
//   const properties = state.wsEvents.download[name]
//   const bytesReceived = properties.bytesReceived || 0
//   const size = properties.size || 0
//   const percentage = Math.round(bytesReceived / size * 100)
//   return h('span',
//     `${properties.bytesReceived || 0} of ${properties.size || 0} bytes (${percentage}%, ${properties.kbps} kbps).`,
//     progressBar(percentage)
//   )
// }
//
// function displayUploadedFile (file) {
//   return h('tr',
//     h('td', h('a', { href: `#files/${file.hash}` }, h('code.text-reset', file.filename))),
//     h('td', displayPeer(file.to)),
//     h('td', h('small', new Date(parseInt(file.timestamp)).toLocaleString()))
//   )
// }

// function showOrHideMedia ({ hash, src, type }) {
//   const playerOptions = { controls: true, autoplay: true }
//   if (state.itemPlaying === hash) {
//     state.itemPlaying = false // only play once
//     return h(type.split('/')[0], playerOptions, h('source', { src, type }))
//   }
//
//   function startPlaying () {
//     state.itemPlaying = hash
//     emit('render')
//   }
//
//   return h('button.btn', { onclick: startPlaying, title: 'Play media' }, icons.use('caret-right-square'))
// }
//
// function displayMedia (file) {
//   const hostAndPort = `${state.connectionSettings.host}:${state.connectionSettings.port}`
//   const src = `${hostAndPort}/downloads/${file.hash}`
//   const type = file.mimeType
//
//   if (IMAGE_TYPES.includes(type)) return h('img', { src, width: 200, alt: file.filename })
//   if (AUDIO_VIDEO_TYPES.includes(type)) return showOrHideMedia({ hash: file.hash, src, type })
//   return h('a.btn.btn-outline-secondary', { href: src, target: '_blank' }, h('small', 'Open in browser'))
// }
//
// function displayPeer (feedId) {
//   const peer = state.peers.find(p => p.feedId === feedId)
//   if (!peer) return feedId
//   return h('a', { href: `#peers/${peer.feedId}` }, icons.use('person'), peer.name || peer.feedId)
// }
//
// function unrequest (files) {
//   if (!Array.isArray(files)) files = [files]
//   return function () {
//     request.delete('/request', { data: { files } })
//       .then((res) => {
//         emit('transfers')
//       })
//       .catch(console.log) // TODO
//   }
// }
  //
// function progressBar (perc) {
  //   return h('div.progress',
//     h('div.progress-bar.bg-success', { role: 'progressbar', style: `width: ${perc}%`, 'aria-valuenow': perc, 'aria-valuemin': '0', 'aria-valuemax': '100' }, perc)
  //   )
  // }
//
  // function namesSort (a, b) {
//   const A = Array.isArray(a.filename) ? a.filename[0] : ''
  //   const B = Array.isArray(b.filename) ? b.filename[0] : ''
//   if (A < B) return -1
  //   if (A > B) return 1
//   return 0
// }
