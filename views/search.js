const basic = require('./basic')
const icons = require('../icons')
// const { readableBytes } = require('../util')
const displayFiles = require('../components/item')
const h = require('hyperscript')
const TITLE = 'harddrive-party - search'

module.exports = function (state, emit) {
  if (state.title !== TITLE) emit(state.events.DOMTITLECHANGE, TITLE)

  state.selectedSearchResult = state.selectedSearchResult ||
    Object.keys(state.searchResults)[0]

  const currentSearchResults = state.searchResults[state.selectedSearchResult] || []

  state.searchterm = state.searchterm || ''

  function submitSearch (e) {
    e.preventDefault()
    emit('search', state.searchterm)
    state.searchterm = ''
  }

  return basic(state, emit,
    h('div.container',
      // h('p', JSON.stringify(state.searchResults)),
      h('form.form-inline.my-2.mb-2', { onsubmit: submitSearch },
        h('div.input-group.mb3',
          h('input.form-control', {
            type: 'search',
            id: 'searchterm',
            value: state.searchterm,
            name: 'searchterm',
            oninput: (event) => {
              state.searchterm = event.target.value
            },
            'aria-label': 'Search',
            placeholder: 'Search'
          }),
          h('div.input-group-append',
            h('button.btn.btn-outline-success', { type: 'submit', class: 'button-addon2' }, icons.use('search'))
          )
        )
      ),
      h('div.card',
        h('div.card-header.pb-0.pr-0',
          h('ul.nav.nav-tabs',
            Object.keys(state.searchResults).map((s) => {
              const active = state.selectedSearchResult === s ? '.active' : ''
              return h('li.nav-item', h(
                `a.nav-link${active}`,
                {
                  onclick: () => {
                    state.selectedSearchResult = s
                    emit('render')
                  }
                },
                h('code.text-reset', `'${JSON.parse(s).searchterm}'`)
              ))
            })
          )
        ),
        h('div.card-body',
          h('table.table.table-hover.table-sm',
            h('tbody',
              currentSearchResults.map(displayFiles(state, emit))
              // currentSearchResults.map((file) => {
              //   return displayFile(file.name, file.size, file.isDir)
              // TODO use some common component from tree-table
              // return h(
              //   'tr',
              //   h(
              //     'td',
              //     icons.use(r.isDir ? 'folder' : 'file'),
              //     h('code.text-reset.ml-1', r.name)
              //   ),
              //   h('td', readableBytes(r.size))
              // )
              // })
            )
          )
        )
      )
    )
  )
}
