const h = require('hyperscript')
const icons = require('../icons')
const { spinner } = require('../components')
const { readableBytes } = require('../util')

module.exports = function (state, emit, content) {
  const sharedFilesSize = state.files['/']
    ? readableBytes(state.files['/'].filter(entry => entry.name === state.name).map(entry => entry.size)[0])
    : '?'
  const downloading = undefined // TODO
  const numberConnectedPeers = state.files['/'] ? state.files['/'].length - 1 : 0

  return h('body',
    h('div.container',
      h('nav.navbar.navbar-expand-lg.navbar-light.bg-light',
        h('p.navbar-brand', { title: 'harddrive-party' }, icons.logo()),
        h('ul.navbar-nav.mr-auto',

          h(`li.nav-item${(state.route === 'connection') ? '.active' : ''}`,
            { title: `Connected to ${state.swarms.connected.length} swarm${state.swarms.connected.length === 1 ? '' : 's'}` },
            h('a.nav-link', { href: '#connection' },
              icons.use('hdd-network'),
              ' connections ',
              h('small', h('strong', state.swarms.connected.length))
            )
          ),

          h(`li.nav-item${(state.route === '/') ? '.active' : ''}`,
            { title: `${numberConnectedPeers} connected peers` }, // TODO
            h('a.nav-link', { href: '#' },
              icons.use('people'),
              ' peers ',
              h('small', h('strong', numberConnectedPeers))
            )
          ),

          h(`li.nav-item${(state.route === 'shares') ? '.active' : ''}`,
            { title: `Sharing ${sharedFilesSize}` },
            h('a.nav-link', { href: '#shares' },
              icons.use('server'),
              ' shares ',
              h('small', h('strong', sharedFilesSize))
            )
          ),

          // h(`li.nav-item${(state.route === 'peers') ? '.active' : ''}`,
          //   { title: numberConnectedPeers === 0
          //     ? 'No connected peers'
          //     : `Connected to ${numberConnectedPeers} peer${numberConnectedPeers === 1 ? '' : 's'}`
          //   },
          //   h('a.nav-link', { href: '#peers' },
          //     icons.use('people'),
          //     ' peers ',
          //     h('small', h('strong', numberConnectedPeers))
          //   )
          // ),

          h(`li.nav-item${(state.route === 'search') ? '.active' : ''}`,
            { title: 'Search for files' },
            h('a.nav-link', { href: '#search' },
              icons.use('search'),
              ' search '
            )
          ),

          h(`li.nav-item${(state.route === 'transfers') ? '.active' : ''}`,
            { title: downloading ? 'Downloading...' : 'Uploads and downloads' },
            h('a.nav-link', { href: '#transfers' },
              icons.use('arrow-down-up'),
              ' transfers ',
              downloading ? spinner() : undefined
            )
          ),

          h(`li.nav-item${(state.route === 'settings') ? '.active' : ''}`,
            h('a.nav-link', { href: '#settings' },
              icons.use('gear'),
              ' settings'
            )
          )
        ),
        h('small', h('code.text-reset', state.name))
        // )
      )

    ),
    h('div.container',
      state.connectionError ? connectionError : undefined,
      content
    ),
    icons.build()
  )

  function connectionError () {
    return h('div',
      h('h3.bg-danger', `Connection lost to the harddrive-party instance over Websocket on
        ${state.connectionSettings.host}:${state.connectionSettings.port}.`),
      h('p', JSON.stringify(state.connectionError)),
      h('h4', `Attempting to reconnect to: ${state.connectionSettings.host}:${state.connectionSettings.port}? `)
    )
  }
}
